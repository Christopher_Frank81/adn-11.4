﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            Console.ReadLine();

            Program.Serialize(business);
        }

        public static void Serialize(Business business)
        {
            XmlSerializer serializerObj = new XmlSerializer(typeof(Business));

            TextWriter writeFileStream = new StreamWriter(@"C:\ADN\Business.xml");

            serializerObj.Serialize(writeFileStream, business);

            writeFileStream.Close();
        }
    }
}
