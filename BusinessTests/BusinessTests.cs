using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Test_Driven_Development;

namespace BusinessTests
{
    [TestClass]
    public class BusinessTests
    {
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void SecondJobIncomplete()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 17));
            

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted && business.Jobs[1].JobCompleted == false);
        }

        [TestMethod]
        public void PaymentWhenNotEnoughHoursToCompleteOneJob()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            Employee e = new Employee(10, 12);
            business.AddEmployee(e);
            decimal payTest = 120;

            // Act
            business.DoWork();
            decimal pay = e.Paycheck;

            // Assert
            Assert.AreEqual(pay, payTest);
        }

        [TestMethod]
        public void PaymentWhenEnoughHoursToCompleteOneJob()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            Employee e = new Employee(10, 17);
            business.AddEmployee(e);
            decimal payTest = 150;

            // Act
            business.DoWork();
            decimal pay = e.Paycheck;

            // Assert
            Assert.AreEqual(pay, payTest);
        }

        [TestMethod]
        public void JobCostTotal()
        {
            // Arrange
            Business business = new Business();
            Job j = new Job(16);
            business.AddJob(j);
            
            Employee e = new Employee(10, 18);
            business.AddEmployee(e);
            decimal jobCostTest = 240;

            // Act
            business.DoWork();
            decimal jobCostActual = j.JobCost;

            // Assert
            Assert.AreEqual(jobCostTest, jobCostActual);
        }
    }
}
